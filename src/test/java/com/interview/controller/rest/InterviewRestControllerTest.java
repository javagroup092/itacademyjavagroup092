package com.interview.controller.rest;

import com.interview.model.Interview;
import com.interview.model.Interviewer;
import com.interview.service.InterviewService;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.*;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static com.jayway.restassured.http.ContentType.JSON;

/**
 *
 * @author Artem Baranovskiy
 */
public class InterviewRestControllerTest extends InterviewerRestIntegrationTest {

    private final String INTERVIEWS_REQUEST = "/rest/interviews";

    @Autowired
    private InterviewService interviewService;

    protected Interview interviewForRemove;

    @BeforeClass
    public void initInterview() {
        interviewForRemove = interviewService.addInterview(new Interview(first));
        RestAssured.port = port;
    }

    @Test
    public void canGetAllInterviews() throws Exception {
        given()
                .contentType(JSON)
        .when()
                .get(INTERVIEWS_REQUEST)
        .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void canReadExistedInterview() throws Exception {
        given()
                .contentType(JSON)
        .when()
                .get(String.format("/rest/interviews/%s", interviewForRemove.getId()))
        .then()
                .statusCode(HttpStatus.SC_OK)
                .body("interviewer", notNullValue())
                .body("comments", nullValue())
                .body("questions", nullValue())
                .body("finalValue", is(0f));
    }

    @Test
    public void canNotReadNonexistentInterview() throws Exception {
        given()
                .contentType(JSON)
        .when()
                .get("/rest/interviews/00000")
        .then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    public void canCreateInterviewWithExistedInterviewer() throws Exception {
        given()
                .contentType(JSON)
                .body(new Interview(second))
        .when()
                .post(INTERVIEWS_REQUEST)
        .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void canNotCreateInterviewWithNonexistentInterviewer() {
        given()
                .contentType(JSON)
                .body(new Interview(new Interviewer()))
        .when()
                .post(INTERVIEWS_REQUEST)
        .then()
                .statusCode(HttpStatus.SC_CONFLICT);
    }

    @Test(dependsOnMethods = "canCreateInterviewWithExistedInterviewer")
    public void canUpdateExistedInterview() {
        final String path = String.format("/rest/interviews/%s", interviewForRemove.getId());
        interviewForRemove.addComment("Comment From Test");
        given()
                .contentType(JSON)
                .body(interviewForRemove)
        .when()
                .put(path)
        .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "canUpdateExistedInterview")
    public void canNotUpdateNonexistentInterview() {
        interviewForRemove.addComment("Comment1");
        given()
                .accept(JSON)
                .contentType(JSON)
                .body(interviewForRemove)
        .when()
                .put("/rest/interviews/00032dsdd")
        .then()
                .statusCode(HttpStatus.SC_CONFLICT);
    }

    @Test(dependsOnMethods = {"canReadExistedInterview", "canUpdateExistedInterview"})
    public void canDeleteInterview() throws Exception {
        final String path = String.format("/rest/interviews/%s", interviewForRemove.getId());
        when()
                .delete(path)
        .then()
                .statusCode(HttpStatus.SC_OK);
    }

}
