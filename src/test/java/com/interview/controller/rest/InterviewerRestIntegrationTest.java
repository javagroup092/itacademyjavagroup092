package com.interview.controller.rest;

import com.interview.AbstractRestTest;
import com.interview.model.Interviewer;
import com.interview.service.InterviewerServiceImpl;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.when;

/**
 *
 * @author NSS
 */
public class InterviewerRestIntegrationTest extends AbstractRestTest {

    @Autowired
    InterviewerServiceImpl interviewerService;

    Interviewer first;
    Interviewer second;
    Interviewer forRemove;
    Interviewer toAdd;

    @BeforeClass
    public void setUp() {
        first = new Interviewer("FirstInterviewer", "LastName", "mail@mail", "skype111", "111");
        second = new Interviewer("SecondInterviewer", "LastNameSecond", "mail2@mail", "skype222", "222");
        forRemove = new Interviewer("ForRemoveInterviewer", "LastNameForRemove", "mail2@mail", "skype222", "222");
        toAdd = new Interviewer("ToAddInterviewer", "LastNameToAdd", "mail2@mail", "skype222", "222");
        
        first = interviewerService.addInterviewer(first);
        second = interviewerService.addInterviewer(second);
        forRemove = interviewerService.addInterviewer(forRemove);
        toAdd = interviewerService.addInterviewer(toAdd);

        RestAssured.port = port;
    }

    @AfterClass
    public void tearDown() {
        interviewerService.removeInterviewer(first.getId());
        interviewerService.removeInterviewer(second.getId());
    }

    @Test
    public void canGetInterviewerById() {
        String firstId = first.getId();

        when().
                get("/rest/interviewers/{id}", firstId).
                then().
                statusCode(HttpStatus.SC_OK).
                body("firstName", Matchers.is("FirstInterviewer")).
                body("id", Matchers.is(firstId));
    }

    @Test
    public void canGetAllInterviewers() {
        when().
                get("/rest/interviewers").
                then().
                statusCode(HttpStatus.SC_OK).
                body("firstName", Matchers.hasItems("FirstInterviewer", "SecondInterviewer"));
    }

    @Test
    public void canDeleteInterviewer() {
        when()
                .delete("/rest/interviewers/{id}", forRemove.getId()).
                then().
                statusCode(HttpStatus.SC_NO_CONTENT);
    }
    
    @Test
    public void canAddInterviewer() {
        when()
                .post("/rest/interviewers/add")
                .then()
                .statusCode(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE);
    }
}
