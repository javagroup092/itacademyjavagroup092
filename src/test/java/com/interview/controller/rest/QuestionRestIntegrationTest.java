package com.interview.controller.rest;

import com.interview.AbstractRestTest;
import com.interview.model.Question;
import com.interview.service.QuestionServiceImpl;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.*;

import static com.jayway.restassured.RestAssured.when;
import static com.jayway.restassured.RestAssured.given;

/**
 * @author Anton Kruglikov.
 */

class QuestionRestIntegrationTest extends AbstractRestTest {

    /*@Autowired
    private QuestionServiceImpl questionService;
    private static final Question questionFirst = new Question("Hibernate", 5.0);
    private static final Question questionSecond = new Question("Spring", 10.34);
    private static final Question questionThird = new Question("Collections", 4.2);
    private static final String QUESTIONS = "/rest/questions/";
    private static final String QUESTION = "/rest/questions/{id}";
    private static final String ADD_QUESTION = "/rest/questions/add";
    private static final int NON_EXISTING_ID = 999;

    @BeforeClass
    public void setUp() {
        questionService.addQuestion(questionFirst);
        questionService.addQuestion(questionSecond);
        RestAssured.port = port;
    }

    @AfterClass
    public void tearDown() {
        questionService.removeQuestion(questionFirst.getId());
        questionService.removeQuestion(questionSecond.getId());
        questionService.removeQuestion(questionThird.getId());
    }

    @Test
    public void getAllQuestionsShouldReturnOK() {
        given().
           contentType(JSON).
        when().
           get(QUESTIONS).
        then().
           statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void getAllQuestionsOnEmptyCollectionShouldReturnNoContent() {
        questionService.removeQuestion(questionFirst.getId());
        questionService.removeQuestion(questionSecond.getId());
        given().
           contentType(ContentType.JSON).
        when().
           get(QUESTIONS).
        then().
           statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    public void getQuestionByIdShouldReturnQuestionById() {
        String id = questionFirst.getId();

        given().
           contentType(JSON).
        when().
           get(QUESTION, id).
        then().
           statusCode(HttpStatus.SC_OK).
           body("questionString", Matchers.is("Hibernate")).
           body("maxQuestionValue", Matchers.is(5.0));
    }

    @Test
    public void getQuestionByNonExistingIdShouldReturnNoFound() {
        given().
           contentType(JSON).
        when().
           get(QUESTION, NON_EXISTING_ID).
        then().
           statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void addNewQuestionShouldReturnStatusOk() {
        given().
           body(questionThird).
           contentType(JSON).
        when().
           post(ADD_QUESTION).
        then().
           statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void addNewQuestionWithoutBodyShouldReturnBadRequest() {
        given().
          contentType(JSON).
        when().
           post(ADD_QUESTION).
        then().
           statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void editQuestionShouldReturnStatusOk() {
        given().
           body(questionFirst).
           contentType(JSON).
        when().
           post(QUESTION, questionFirst.getId()).
        then().
           statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void editQuestionWithoutBodyShouldReturnBadRequest() {
        given().
           contentType(JSON).
        when().
           post(QUESTION, questionFirst.getId()).
        then().
           statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void deleteQuestionShouldReturnStatusOk() {
        given().
           contentType(JSON).
        when().
           delete(QUESTION, questionFirst.getId()).
        then().
           statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void getQuestionAfterDeletingShouldReturnNoContent() {
        questionService.removeQuestion(questionFirst.getId());
        given().
           contentType(ContentType.JSON).
        when().
           get(QUESTION, questionFirst.getId()).
        then().
           statusCode(HttpStatus.SC_NO_CONTENT);
    }*/

}
