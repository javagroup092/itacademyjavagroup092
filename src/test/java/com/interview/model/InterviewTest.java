package com.interview.model;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;

import static org.testng.Assert.*;

public class InterviewTest {

    private Interview empty;
    private Interview full;

    @BeforeMethod
    public void initialize() {
        empty = new Interview();
        final Interviewer interviewer = new Interviewer("FirstName", "LastName", "Email", "Skype", "Phone");
        full = new Interview(new HashSet<InterviewQuestion>(), interviewer, 150.0, 200.0, new ArrayList<String>());
        full.setId("id");
    }

    @Test
    public void twoSameObjectsEqualsEachOther() {
        assertEquals(empty, new Interview());
        final Interview test = new Interview();
        test.setId("id");
        assertEquals(full, test);
    }

    @Test
    public void twoSameObjectsReturnSameHashCode() {
        assertEquals(new Interview().hashCode(), new Interview().hashCode());
        final Interview test = new Interview();
        test.setId("id");
        assertEquals(full.hashCode(), test.hashCode());
    }

    @Test
    public void notNullToString() {
        assertNotNull(empty.toString());
        assertNotNull(full.toString());
    }

    @Test
    public void lazyCollectionInitializing() {
        assertNull(empty.getQuestions());
        empty.addQuestion(new InterviewQuestion());
        assertNotNull(empty.getQuestions());
        assertNull(empty.getComments());
        empty.addComment("Comment");
        assertNotNull(empty.getComments());
    }

    @Test
    public void notNullSpecifiedFields() {
        assertNotNull(full.getQuestions());
        assertNotNull(full.getInterviewer());
        assertNotNull(full.getComments());
    }

    /*@Test
    public void correctIncreasingFinalValue() {
        final InterviewQuestion notSkipped = new InterviewQuestion(new Question("Question1", 20), 10, false, false);
        final InterviewQuestion skipped = new InterviewQuestion(new Question("Question2", 20), 10, true, false);
        empty.addQuestion(notSkipped);
        empty.addQuestion(skipped);
        assertEquals(empty.getFinalValue(), 10.0);
        assertEquals(empty.getMaxValue(), 20.0);
    }*/


}