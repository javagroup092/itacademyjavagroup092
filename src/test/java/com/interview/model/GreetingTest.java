package com.interview.model;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class GreetingTest {

    private Greeting greeting;

    @BeforeClass
    public void initialize() {
        greeting = new Greeting("testId", "TestMessage");
    }

    @Test
    public void testGreetingObject() {
        assertNotNull(greeting);
    }

    @Test
    public void greetingContentMustNotBeNullAfterCreating() {
        assertNotNull(greeting.getMessage());
    }

    @Test
    public void greetingIdMustNotBeNullAfterCreating() {
        assertNotNull(greeting.getId());
    }

}