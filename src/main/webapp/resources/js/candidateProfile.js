$(document).ready(function() {
    var id = $("#idHidden").val();
    $.ajax({
        type: "GET",
        url: "/rest/candidates/" + id,
        success: function(result) {
            console.log("RESULT: " + JSON.stringify(result));
            var candidate = JSON.stringify(result);
            candidate = JSON.parse(candidate);
            var surname = candidate.surname;
            var name = candidate.name;
            var patronymic = candidate.patronymic;
            var mail = candidate.email;
            var skype = candidate.skype;
            var phone = candidate.phoneNumber;
            $("#surname").val(surname);
            $("#name").val(name);
            $("#patronymic").val(patronymic);
            $("#eMail").val(mail);
            $("#skype").val(skype);
            $("#phone").val(phone);

        }
    });
});

/*
private String surname;
private String name;
private String patronymic;
private List<Interview> interviewList;
private Calendar date;
private EnglishLevel englishLevel;
private String phoneNumber;
private String email;
private String skype;
Candidate
56f3da3d24e569664371030a
*/


