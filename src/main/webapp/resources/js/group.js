$(document).ready(function () {
    var id = $("#idHidden").val();
    var dates = [];
    var strDates = [];

    getGroupDates(id);

    $(".dayRow").click(function() {
        var x = $(this).attr("id");
        var sd = x.substr(1);
        if ($("." + sd).is(":visible")) {
            $("." + sd).hide();
        } else {
            $("." + sd).show();
        }
    });

    function getGroup(id) {
         $.ajax({
             type: "GET",
             url: "/rest/groups/" + id,
             success: function (result) {
                 var group = JSON.stringify(result);
                 group = JSON.parse(group);
                 $("#groupNameSpan").text(group.name);
             },
             async: false
         });
    }

    function getGroupDates(id) {
        dates = [];
        strDates = [];

        getGroup(id);

        $.ajax({
            type: "GET",
            url: "/rest/groups/" + id + "/dates",
            success: function (result) {
                $.each(result, function (i, strDate) {
                    var date = new Date(+strDate);
                    strDates.push(strDate);
                    dates.push(date);
                });
                $.each(dates, function (i, date) {
                    var showDate = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
                    $("#dayTable").append('<tr id="diw-' + strDates[i] +
                            '" class="dayRow"><td colspan="3" style="background-color: #2f4f4f">' + showDate +
                        '</td></tr>');
                    getGroupCandidatesByDate(id, strDates[i]);
                });
            },
            async: false
        });
    }

    function getGroupCandidatesByDate(id, strDate) {
        $.ajax({
            type: "GET",
            url: "/rest/groups/" + id + "/candidatesByDate/" + strDate,
            success: function (result) {
                var candidates = JSON.stringify(result);
                candidates = JSON.parse(candidates);
                $.each(candidates, function (i, candidate) {
                    var candTime = getCandidateTimeString(new Date(candidate.date));
                    var fullName = candidate.surname + " " + candidate.name;
                    $("#dayTable").append('<tr class="iw-'+ strDate +'"><td><a href="#">' + fullName +
                    '</a></td><td>' + candTime +
                    '</td><td><a href="#interview"><img src="../../../resources/images/icons/Skull-48.png" ' +
                    'alt="&gt;" style="height: 1.5em" title="interview"/></a></td></tr>');
                });
            },
            async: false
        });
    }

    function getCandidateTimeString(date) {
        var minutes = "";
        if (date.getMinutes() < 10) {
            minutes = '0' + date.getMinutes();
        } else {
            minutes = date.getMinutes();
        }
        return date.getHours() + ':' + minutes;
    }
});
