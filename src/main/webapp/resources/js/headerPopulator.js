$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/rest/groups/id",
        success: function(result) {
            var groups = JSON.stringify(result);
            groups = JSON.parse(groups);

            $("#hdrDropdownSchedule").html("");
            $.each(groups, function(id, name) {
                console.log("------------------------------" + name);
                $("#hdrDropdownSchedule").append('<li><a href="/web/groups/' + id + '">' + name + '</a></li>');
            });
        }
    });
});

