$(document).ready(function () {

    getDay(getParameterByName('date'), getParameterByName('direction'));

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        url = url.toLowerCase(); // This is just to avoid case sensitiveness
        name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// This is just to avoid case sensitiveness for query parameter name
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function getDay(dateStr, directStr) {
        $.ajax({
            type: "GET",
            url: "/rest/day?date=" + dateStr + "&direction=" + directStr,
            success: function (result) {
                console.log("RESULT: " + JSON.stringify(result));
                $("#dayTable > tbody").html("");
                var day = JSON.stringify(result);
                day = JSON.parse(day);

                var dayDate = new Date(day.date);
                var date = dayDate.getDate() + "-" + (dayDate.getMonth() + 1) + "-" + dayDate.getFullYear();

                $("#dateSpan").text(date);
                $("#directionSpan").text(day.direction);


                $.each(day.candidates, function (i, candidate) {
                    var candDate = new Date(candidate.calendar);
                    var minutes = "";
                    if (candDate.getMinutes() < 10) {
                        minutes = '0' + candDate.getMinutes();
                    } else {
                        minutes = candDate.getMinutes();
                    }
                    var candTime = candDate.getHours() + ':' + minutes;
                    $("#dayTable").append('<tr><td>' + candidate.name +
                    '</td><td>' + candTime +
                    '</td><td><a href="#interview"><img src="../../../resources/images/icons/Skull-48.png" ' +
                    'alt="&gt;" style="height: 1.5em" title="interview"/></a></td></tr>');
                });
            },
            error: function () {
                console.log("Error");
            }
        });
    }
});
