$(document).ready(function() {
    var id = $("#idHidden").val();
     $.ajax({
         type: "GET",
         url: "/rest/groups/" + id,
         success: function (result) {
             var group = JSON.stringify(result);
             group = JSON.parse(group);
             $("#groupNameSpan").text(group.name);
         },
         async: false
     });
    $.ajax({
        type: "GET",
        url: "/rest/groups/" + id + "/groupReport/",
        success: function (result) {
            console.log("RESULT: " + JSON.stringify(result));
            var candidates = JSON.stringify(result);
            candidates = JSON.parse(candidates);
            $.each(candidates, function (i, candidate) {
                var candRank = 100;

                $("#reportTable").append('<tr><td>' + candidate.surname + " " + candidate.name +
                '</td><td>' + candRank +
                '</td></tr>');
            });
        },
        async: false
    });
});

