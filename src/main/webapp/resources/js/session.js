$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/rest/session",
        success: function(result) {
            console.log("RESULT: " + JSON.stringify(result));
            var session = JSON.stringify(result);
            session = JSON.parse(session);
            var dates = session.dates;
            var directions = session.directions;
            var days = session.days;
            console.log("DATES: " + dates);
            console.log("DIRECTIONS: " + directions);
            console.log("DAYS:" + days);

            $("#sessionDaysTable > tbody").html("");
            $.each(days, function(i, day) {
                var dayDate = new Date(day.date);
                var date = dayDate.getDate() + "-" + (dayDate.getMonth() + 1) + "-" + dayDate.getFullYear();
                var direction = day.direction;
                var link = '"/web/session/day?date=' + date + '&direction=' + direction + '"';
                console.log("LINK: " + link);
                $("#sessionDaysTable").append('<tr><td>' + direction +
                '</td><td>' + date +
                '</td><td><a href='+ link + '><img src="../../../resources/images/icons/Clock-50.png" ' +
                'alt="&gt;" style="height: 1.5em" title="day"/></a></td></tr>');
            });
        }
    });
});

