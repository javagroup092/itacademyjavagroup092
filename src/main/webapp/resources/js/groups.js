$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/rest/groups/id",
        success: function(result) {
            console.log("RESULT: " + JSON.stringify(result));
            var groups = JSON.stringify(result);
            groups = JSON.parse(groups);

            $("#sessionDaysTable > tbody").html("");
            $.each(groups, function(id, name) {
                var sel = '<select class="form-control btn btn-default selectTemplate"><option>none</option></select>';
                $("#sessionDaysTable > tbody").append('<tr><td style="vertical-align: middle">' + name +
                    '</td><td>' + sel +
                    '</td><td style="vertical-align: middle"><a href="/web/groups/' + id + '"><img src="../../../resources/images/icons/Clock-50.png" ' +
                    'alt="&gt;" style="height: 1.8em" title="day"/></a></td></tr>');
            });
            $.ajax({
                type: "GET",
                url: "/rest/templates/",
                success: function(result) {
                    var templates = JSON.stringify(result);
                    templates = JSON.parse(templates);
                    console.log("TEMPLATES JSON: " + templates);
                    $.each(templates, function(i, template) {
                        $(".selectTemplate").append('<option>' + template.name + '</option>');
                    });
                }
            });
        }
    });


});

