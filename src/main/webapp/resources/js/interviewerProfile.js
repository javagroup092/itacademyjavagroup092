$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/rest/interviewers/56f1c9fc9ac7adc26eafe54f",
        success: function(result) {
            console.log("RESULT: " + JSON.stringify(result));
            var interviewer = JSON.stringify(result);
            interviewer = JSON.parse(interviewer);
            var firstName = interviewer.firstName;
            var lastName = interviewer.lastName;
            var mail = interviewer.mail;
            var skype = interviewer.skype;
            var phone = interviewer.phone;
            $("#fName").val(firstName);
            $("#lName").val(lastName);
            $("#eMail").val(mail);
            $("#skype").val(skype);
            $("#phone").val(phone);

        }
    });
});


