<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Admin Console</title>
        <spring:url value="/styles/standard.css" var="cssURL"/>
        <link rel="stylesheet" type="text/css" href="${cssURL}" />
    </head>
    <body>
        <div id="userinfo">
            <br/>
            <h1><p><a href="<spring:url value="/web/admin/login" />">Sing-In</a></p></h1>
        </div>
    </body>
</html>