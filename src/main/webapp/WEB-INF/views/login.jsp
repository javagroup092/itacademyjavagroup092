<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Interview Login Page </title>
        <spring:url value="/styles/standard.css" var="cssURL"/>
        <link rel="stylesheet" type="text/css" href="${cssURL}" />
    </head>
    <body>
            <form action="/web/admin/login" method="POST">
                <table>
                    <caption align="left">Login:</caption>
                    <tr>
                        <td>User Name:</td>
                        <td><input type="text" name="username"/></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type="password" name="password"/></td>
                    </tr>
                    <tr>
                        <td>Remember me</td>
                        <td>
                            <input id="remember_me" name="remember_me"
                                   type="checkbox"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input name="submit" type="submit" value="Sign In"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>