<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Day</title>

    <script src="http://code.jquery.com/jquery-latest.js"></script>

    <script>
        $(document).ready(function () {
            $("#dayFormSubmit").click(function () {
                var dateStr = $("#dateInput").val();
                var directStr = $("#directionInput").val();

                console.log("DATE STRING: " + dateStr);
                console.log("DIR STRING:  " + directStr);

                getDay(dateStr, directStr);
            });

            function getDay(dateStr, directStr) {
                $.ajax({
                    type: "GET",
                    url: "/rest/day?date=" + dateStr + "&direction=" + directStr,
                    success: function (result) {
                        console.log("RESULT: " + JSON.stringify(result));
                        $("#dayTable > tbody").html("");
                        var day = JSON.stringify(result);
                        day = JSON.parse(day);

                        var dayDate = new Date(day.date);
                        var date = dayDate.getDate() + "-" + (dayDate.getMonth() + 1) + "-" + dayDate.getFullYear();

                        $("#dateSpan").text(date);
                        $("#directionSpan").text(day.direction);


                        $.each(day.candidates, function (i, candidate) {
                            var candDate = new Date(candidate.calendar);
                            var minutes = "";
                            if (candDate.getMinutes() < 10) {
                                minutes = '0' + candDate.getMinutes();
                            } else {
                                minutes = candDate.getMinutes();
                            }
                            var candTime = candDate.getHours() + ':' + minutes;
                            $("#dayTable").append('<tr><td>' + candidate.name +
                            '</td><td>' + candTime +
                            '</td>');
                        });
                    },
                    error: function () {
                        console.log("Error");
                    }
                });
            }
        });
    </script>
</head>
<body>
<h2>Fill the form</h2>

<div id="dayForm">
    <table>
        <tr>
            <td>Date:</td>
            <td><input id="dateInput" type="text" placeholder="date"/></td>
        </tr>
        <tr>
            <td>Direction:</td>
            <td><input id="directionInput" type="text" placeholder="direction"></td>
        </tr>
        <tr>
            <td colspan="2">
                <button id="dayFormSubmit" type="button">Find</button>
            </td>
        </tr>
    </table>
</div>
<div>
    <br/>
    <br/>

    <h2>Day</h2>
    <h4>Date: <span id="dateSpan" style="color: blue"></span></h4>
    <h4>Direction: <span id="directionSpan" style="color: blue"></span></h4>
    <table id="dayTable">
        <thead>
        <tr>
            <th>Name</th>
            <th>Time</th>
        </tr>
        </thead>
        <tbody>
        <!-- Populated by JS -->
        </tbody>
    </table>
</div>
</body>
</html>
