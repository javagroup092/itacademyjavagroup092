<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Session</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css"/>

    <script src="../../../resources/jquery/jquery-2.2.2.min.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

    <script src="../../../resources/js/session.js"></script>
</head>
<body>
    <div class="container">
    <h1>Schedule</h1>
    <table id="sessionDaysTable" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Direction</th>
                <th>Date</th>
                <th style="width: 10em">Check</th>
            </tr>
        </thead>
        <tbody>
        <!-- Populated by JS -->
        </tbody>
    </table>
    </div>
</body>
</html>
