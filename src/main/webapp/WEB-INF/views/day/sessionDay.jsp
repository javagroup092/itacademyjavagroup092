<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Day</title>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css"/>

    <script src="../../../resources/jquery/jquery-2.2.2.min.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

    <script src="../../../resources/js/sessionDay.js"></script>
</head>
<body>
<div class="container">
    <h2>Day</h2>
    <h4 style="padding-bottom: 2em">
        Date: <span id="dateSpan" style="color: #add8e6; padding-right: 2em"></span>
        Direction: <span id="directionSpan" style="color: #add8e6"></span>
    </h4>
    <table id="dayTable" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Time</th>
            <th style="width: 10em">Interview</th>
        </tr>
        </thead>
        <tbody>
        <!-- Populated by JS -->
        </tbody>
    </table>
    <a href="/web/session" style="padding-top: 2em; color: #add8e6">Back to the session</a>
</div>

</body>
</html>
