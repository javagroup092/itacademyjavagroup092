<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
    <spring:url value="/styles/standard.css" var="cssURL"/>
    <link rel="stylesheet" type="text/css" href="${cssURL}" />
    
    <spring:message code="label_login" var="labelLogin"/>
    <spring:url value="/web/admin/login" var="loginUrl"/>
    
    You are now logged out!!
    
    <a href="${loginUrl}">${labelLogin}</a> 
</div>