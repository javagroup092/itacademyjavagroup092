<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
    <spring:message code="label_interviewer_info" var="labelInterviewerInfo"/>
    <spring:message code="label_interviewer_first_name" var="labelInterviewerFirstName"/>
    <spring:message code="label_interviewer_last_name" var="labelInterviewerLastName"/>
    <spring:message code="label_interviewer_email" var="labelInterviewerEmail"/>
    <spring:message code="label_interviewer_phone" var="labelInterviewerPhone"/>
    <spring:message code="label_interviewer_skype" var="labelInterviewerSkype"/>
    <spring:message code="label_interviewer_update" var="labelInterviewerUpdate"/>
    
    <spring:url value="/interviewer" var="editnIterviewerUrl"/>
    <spring:url value="/styles/standard.css" var="cssURL"/>
    <link rel="stylesheet" type="text/css" href="${cssURL}" />
    
    <h1>${labelInterviewerInfo}</h1>
    <div id="contactInfo">
        <table>
            <tr>
                <td>ID</td>
                <td>${interviewer.getId()}</td>
            </tr>
            <tr>
                <td>${labelInterviewerLastName}</td>
                <td>${interviewer.lastName}</td>
            </tr>
            <tr>
                <td>${labelInterviewerFirstName}</td>
                <td>${interviewer.firstName}</td>
            </tr>
            <tr>
                <td>${labelInterviewerEmail}</td>
                <td>${interviewer.mail}</td>
            </tr>
            <tr>
                <td>${labelInterviewerPhone}</td>
                <td>${interviewer.phone}</td>
            </tr>
            <tr>
                <td>${labelInterviewerSkype}</td>
                <td>${interviewer.skype}</td>
            </tr>
        </table>
        <a href="${editInterviewerUrl}${interviewer.id}?form">${labelInterviewerUpdate}</a>
    </div>
</div>