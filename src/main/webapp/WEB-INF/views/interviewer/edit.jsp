<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
    <spring:message code="label_interviewer_new" var="labelInterviewerNew"/>
    <spring:message code="label_interviewer_update" var="labelInterviewerUpdate"/>
    <spring:message code="label_interviewer_first_name" var="labelInterviewerFirstName"/>
    <spring:message code="label_interviewer_last_name" var="labelInterviewerLastName"/>
    <spring:message code="label_interviewer_email" var="labelInterviewertEmail"/>
    <spring:message code="label_interviewer_phone" var="labelInterviewerPhone"/>
    <spring:message code="label_interviewer_skype" var="labelInterviewerSkype"/>

    <spring:url value="save" var="saveURL"/>
    <spring:url value="/styles/standard.css" var="cssURL"/>
    <link rel="stylesheet" type="text/css" href="${cssURL}" />

    <spring:eval expression="interviewer.id == null ? labelInterviewerNew:labelInterviewerUpdate" var="formTitle"/>

    <h1>${formTitle}</h1>

    <div id="interviewerUpdate">
        <form:form  modelAttribute="interviewer" action="${saveURL}" method="POST">

            <c:if test="${not empty interviewer.getId()}">
                <form:label path="id">
                    ID 
                </form:label>
                <form:input path="id" />   
                <p/>
            </c:if>

            <form:label path="lastName">
                ${labelInterviewerLastName}* 
            </form:label>
            <form:input path="lastName" /> 
            <p/>

            <form:label path="firstName">
                ${labelInterviewerFirstName}* 
            </form:label>
            <form:input path="firstName" />
            <p/>

            <form:label path="mail">
                ${labelInterviewertEmail}*
            </form:label>
            <form:input path="mail" id="mail"/>      
            <p/>

            <form:label path="phone">
                ${labelInterviewerPhone}
            </form:label>
            <form:input path="phone" id="phone"/>     
            <p/>

            <form:label path="skype">
                ${labelInterviewerSkype}
            </form:label>
            <form:input path="skype" id="skype"/>    
            <p/>            

            <button type="submit">Save</button> 
            <button type="reset">Reset</button>           

        </form:form>   
    </div>
</div>