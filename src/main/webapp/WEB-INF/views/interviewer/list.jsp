<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
    <spring:message code="label_interviewer_list" var="labelInterviewerList"/>
    <spring:message code="label_interviewer_first_name" var="labelInterviewerFirstName"/>
    <spring:message code="label_interviewer_last_name" var="labelInterviewerLastName"/>
    <spring:message code="label_interviewer_email" var="labelInterviewertEmail"/>
    <spring:message code="label_interviewer_phone" var="labelInterviewerPhone"/>
    <spring:message code="label_interviewer_skype" var="labelInterviewerSkype"/>
    <spring:message code="label_interviewer_delte" var="labelInterviewerDelete"/>
    <spring:message code="menu_add_interviewer" var="menuAddInterviewer"/>
    <spring:message code="label_welcome" var="labelWelcome"/>
    <spring:message code="header_text" var="headerText"/>
    <spring:message code="label_logout" var="labelLogout"/>

    <spring:url value="/web/admin/interviewer" var="homeUrl"/>
     <spring:url value="/web/admin/logout" var="logOutUrl"/>
    <spring:url value="?form" var="addInterviewerUrl"/>
    <spring:url value="delete" var="deleteInterviewerUrl"/>
    <spring:url value="/styles/standard.css" var="cssURL"/>
     
    <link rel="stylesheet" type="text/css" href="${cssURL}" />
    
    <div id="appname">
        <h1>${labelInterviewerList}</h1>
    </div>
    
    <div id="userinfo">
        ${labelWelcome} <c:out value="${pageContext.request.remoteUser}"/>
            <br/>
            <a href="${logOutUrl}">${labelLogout}</a>
    </div>
    
    <div id="userinfo">
        <a href="${homeUrl}/${addInterviewerUrl}"><h3>${menuAddInterviewer}</h3></a>
    </div>

    <c:if test="${not empty interviewers}">
        <table>
            <thead>
                <tr>
                    <th>${labelInterviewerLastName}</th>
                    <th>${labelInterviewerFirstName}</th>
                    <th>${labelInterviewertEmail}</th>
                    <th>${labelInterviewerPhone}</th>
                    <th>${labelInterviewerSkype}</th>
                    <th>${labelInterviewerDelete}</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${interviewers}" var="interviewer">
                    <tr>
                        <td>
                            <a href="${homeUrl}/${interviewer.id}">${interviewer.lastName}</a>
                        </td>
                        <td>${interviewer.firstName}</td>
                        <td>${interviewer.mail}</td>
                        <td>${interviewer.phone}</td>
                        <td>${interviewer.skype}</td>
                        <td>
                            <a href="${homeUrl}/${deleteInterviewerUrl}/${interviewer.id}">${labelInterviewerDelete}</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>