package com.interview.repository;

import com.interview.model.Greeting;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface GreetingRepository extends MongoRepository<Greeting, String> {
    public List<Greeting> findByMessage(String message);
}
