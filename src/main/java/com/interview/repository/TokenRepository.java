package com.interview.repository;

import com.interview.util.Token;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author NSS
 */
@Service
public interface TokenRepository extends MongoRepository<Token, String> {
    public Token findBySeries(String series);
    public Token findByUsername(String username);
}