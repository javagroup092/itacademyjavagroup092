package com.interview.service;

import com.interview.model.Candidate;

import java.io.File;
import java.util.Calendar;
import java.util.List;

public interface CandidateService {
    void addCandidate(Candidate candidate);

    Candidate getCandidate(String id);

    void updateCandidate(Candidate candidate);

    void removeCandidate(String id);

    List<Candidate> candidateList();

    void clearList();
}
