package com.interview.service;

import com.interview.model.Question;

import java.util.List;

/**
 * @author Anton Kruglikov.
 */
public interface QuestionService {

    List<Question> getAllQuestions();
    Question getQuestion(String id);
    void addQuestion(Question question);
    void updateQuestion(Question question);
    void removeQuestion(String id);

}
