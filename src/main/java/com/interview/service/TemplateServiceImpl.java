package com.interview.service;

import com.interview.model.Question;
import com.interview.model.Template;
import com.interview.repository.QuestionRepository;
import com.interview.repository.TemplateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Anton Kruglikov.
 */
@Service
public class TemplateServiceImpl implements TemplateService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TemplateRepository templateRepository;

    @Override
    public List<Template> getAllTemplates() {
        List<Template> templateList = templateRepository.findAll();
        log.info(String.format("List of all templates: %s", templateList));
        return templateList;
    }

    @Override
    public Template getTemplate(String id) {
        Template template = templateRepository.findOne(id);
        if (template == null) {
            log.info(String.format("Template with id '%s' doesn't exists in database.", id));
            return null;
        }
        log.info(String.format("Template '%s' has been received from database", id));
        return template;
    }

    @Override
    public void addTemplate(Template template) {
        Template receivedTemplate = templateRepository.findTemplateByName(template.getName());
        if (receivedTemplate != null) {
            log.info(String.format("Template with name '%s' already exists in database", receivedTemplate.getName()));
        } else {
            log.info(String.format("Template with '%s' has been added. ID:: '%s'", template.getName(), template.getId()));
            template.setId(null);
            templateRepository.save(template);
        }
    }

    @Override
    public void updateTemplate(Template template) {
        if (template == null) {
            log.info(String.format("Template '%s' doesn't exists in database.", template));
        } else {
            log.info(String.format("Template:: '%s' has been updated", template));
            templateRepository.save(template);
        }
    }

    @Override
    public void removeTemplate(String id) {
        Template template = templateRepository.findOne(id);
        if (template == null) {
            log.info(String.format("Template with id '%s' doesn't exists in database.", id));
        } else {
            log.info(String.format("Template with id '%s' has been deleted from database", id));
            templateRepository.delete(template);
        }
    }

    @Override
    public List<Question> getQuestionsFromTemplate(String id) {
        Template template = templateRepository.findOne(id);
        log.info(String.format("List of Questions from %s template: %s", template.getName(), template.getQuestions()));
        return template.getQuestions();
    }

    @Override
    public void addQuestionToTemplate(String templateId, Question question) {
        Template template = templateRepository.findOne(templateId);
        log.info(String.format("Question '%s' has been added to template '%s'", question, template.getName()));
        template.setQuestion(question);
    }

    @Override
    public void deleteQuestionFromTemplate(String templateId, String questionId) {
        Template template = templateRepository.findOne(templateId);
        List<Question> questions = template.getQuestions();
        for (Question question : questions) {
            if (question.getId().equals(questionId)) {
                questions.remove(question);
                log.info(String.format("Question '%s' has been deleted from template '%s'", question, template.getName()));
            }
        }
    }
}
