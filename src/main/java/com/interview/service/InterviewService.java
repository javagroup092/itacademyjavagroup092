package com.interview.service;

import com.interview.model.Interview;

import java.util.List;

/**
 * @author Artem Baranovskiy
 */
public interface InterviewService {

    Interview addInterview(Interview interview);
    Interview updateInterview(Interview interview);
    Interview getInterview(String id);
    void deleteInterview(String id);
    List<Interview> getInterviews();

}
