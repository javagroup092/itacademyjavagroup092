package com.interview.service;

import com.interview.model.Interviewer;
import java.util.List;

/**
 *
 * @author NSS
 */
public interface InterviewerService {
    
    Interviewer addInterviewer(Interviewer interviewer);
    Interviewer updateInterviewer(Interviewer interviewer);
    Interviewer getInterviewer(String id);
    void removeInterviewer(String id);
    List<Interviewer> getInterviewers();


}
