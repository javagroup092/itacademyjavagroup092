package com.interview.service;

import com.interview.model.Interview;
import com.interview.repository.InterviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 *
 * @author NSS
 */
@Service
public class InterviewServiceImpl implements InterviewService {

    @Autowired
    private InterviewRepository repository;

    @Override
    public Interview addInterview(Interview interview) {
        interview.setId(null);
        return repository.insert(interview);
    }

    @Override
    public Interview updateInterview(Interview interview) {
        if (!repository.exists(interview.getId())) {
            return null;
        }
        return repository.save(interview);
    }

    @Override
    public Interview getInterview(String id) {
        return repository.findOne(id);
    }

    @Override
    public void deleteInterview(String id) {
        repository.delete(id);
    }

    @Override
    public List<Interview> getInterviews() {
        return repository.findAll();
    }
}