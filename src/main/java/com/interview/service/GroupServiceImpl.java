package com.interview.service;

import com.interview.model.Candidate;
import com.interview.model.Group;
import com.interview.repository.GreetingRepository;
import com.interview.repository.GroupRepository;
import com.interview.util.GroupTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

/**
 * @author Yegor Gulimov
 */

@Service
public class GroupServiceImpl implements GroupService{

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GroupRepository groupRepository;

    public List<Group> list() {
        List<Group> groups = groupRepository.findAll();
        log.info(String.format("List of all groups: %s", groups));
        return groups;
    }
    
    public Group get(String id) {
        if (!argNotNull(id)) {
            return null;
        }
        Group receivedGroup = groupRepository.findOne(id);
        if (receivedGroup != null) {
            log.info(String.format("Group '%s' received from database by id '%s'",
                    receivedGroup, id));
            return receivedGroup;
        }
        log.info(String.format(
                "Group with id '%s' doesn't exists in database. Nothing to be received",
                id));
        return null;
    }

    @Override
    public Group getByName(String name) {
        if (!argNotNull(name)) {
            return null;
        }
        Group receivedGroup = groupRepository.findGroupByName(name);
        if (receivedGroup != null) {
            log.info(String.format("Group '%s' received from database by name '%s'",
                    receivedGroup, name));
            return receivedGroup;
        }
        log.info(String.format(
                "Group with name '%s' doesn't exists in database. Nothing to be received",
                name));
        return null;
    }

    public Group add(Group group) {
        if (!argNotNull(group)) {
            return null;
        }
        Group receivedGroup = groupRepository.findGroupByName(group.getName());
        if (receivedGroup != null) {
            log.info(String.format(
                    "Group with name '%s' already exists in database. Duplicates aren't allowed",
                    receivedGroup));
            return null;
        }

        Group resultGroup = groupRepository.save(group);
        log.info(String.format("Group '%s' has been saved with id '%s'",
                resultGroup, resultGroup.getId()));
        return resultGroup;
    }

    public boolean remove(String id) {
        if (!argNotNull(id)) {
            return false;
        }
        Group group = groupRepository.findOne(id);
        if (group != null) {
            groupRepository.delete(group);
            log.info(String.format("Group '%s' has been deleted from database", group));
            return true;
        }
        log.info(String.format(
                "Group with id '%s' doesn't exists in database. Nothing to be removed",
                id));
        return false;
    }

    public Group update(String id, Group updatedGroup) {
        if (!argNotNull(id) || !argNotNull(updatedGroup)) {
            return null;
        }
        Group receivedGroup = groupRepository.findOne(id);
        if (receivedGroup != null) {
            updatedGroup.setId(id);
            Group checkGroup = groupRepository.findGroupByName(updatedGroup.getName());
            if (checkGroup != null && !checkGroup.getId().equals(updatedGroup.getId())) {
                log.info(String.format("Group with name '%s' already exists in database with id '%s'. Duplicates aren't allowed", updatedGroup.getName(), checkGroup.getId()));
                return null;
            }
            receivedGroup = updatedGroup;
            Group savedGroup = groupRepository.save(receivedGroup);
            log.info(String.format("Group '%s' has been updated in database", savedGroup));
            return savedGroup;
        }
        log.info(String.format(
                "Group with id '%s' doesn't exists in database. Nothing to be updated",
                id));
        return null;
    }

    public List<Candidate> getGroupCandidatesByDate(String id, Calendar date) {
        if (!argNotNull(id) || !argNotNull(date)) {
            return null;
        }
        Group receivedGroup = groupRepository.findOne(id);
        if (receivedGroup == null) {
            log.info(String.format(
                    "Group with id '%s' doesn't exists in database. Nothing to be received",
                    id));
            return null;
        }
        List<Candidate> receivedGroupCandidates = receivedGroup.getCandidates();
        List<Candidate> appropriateCandidates = new ArrayList<>();
        for (Candidate candidate : receivedGroupCandidates) {
            if (GroupTool.compareDates(candidate.getDate(), date)) {
                appropriateCandidates.add(candidate);
            }
        }

        log.info(String.format(
                "List of candidates of group '%s' with date '%s': %s",
                id, date, appropriateCandidates));

        return appropriateCandidates;
    }

    public List<Calendar> getGroupDates(String id) {
        if (!argNotNull(id)) {
            return null;
        }
        Group receivedGroup = groupRepository.findOne(id);
        if (receivedGroup == null) {
            log.info(String.format(
                    "Group with id '%s' doesn't exists in database. Nothing to be received",
                    id));
            return null;
        }
        List<Candidate> candidates = receivedGroup.getCandidates();

        Set<Calendar> datesSet = GroupTool.getDates(candidates);
        List<Calendar> datesList = new ArrayList<>();
        for (Calendar date : datesSet) {
            datesList.add(date);
        }
        return datesList;
    }

    private boolean argNotNull(Object arg) {
        if (arg == null) {
            log.info("Argument cannot be 'null'");
            return false;
        }
        return true;
    }


}
