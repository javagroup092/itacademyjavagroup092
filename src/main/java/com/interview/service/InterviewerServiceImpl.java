package com.interview.service;

import com.interview.model.Interviewer;
import com.interview.repository.InterviewerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Artem Baranovskiy
 */
@Service
public class InterviewerServiceImpl implements InterviewerService {

    @Autowired
    private InterviewerRepository repository;

    @Override
    public Interviewer addInterviewer(Interviewer interviewer) {
        interviewer.setId(null);
        return repository.insert(interviewer);
    }

    @Override
    public Interviewer updateInterviewer(Interviewer interviewer) {
        return repository.save(interviewer);
    }

    @Override
    public Interviewer getInterviewer(String id) {
        return repository.findOne(id);
    }

    @Override
    public void removeInterviewer(String id) {
        repository.delete(id);
    }

    @Override
    public List<Interviewer> getInterviewers() {
        return repository.findAll();
    }
}
