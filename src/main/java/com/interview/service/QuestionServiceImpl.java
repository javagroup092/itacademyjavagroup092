package com.interview.service;

import com.interview.model.InterviewQuestion;
import com.interview.model.Question;
import com.interview.repository.QuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Anton Kruglikov.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    QuestionRepository questionRepository;

    @Override
    public List<Question> getAllQuestions() {
        List<Question> questionList = questionRepository.findAll();
        log.info(String.format("List of all questions: %s", questionList));
        return questionList;
    }

    @Override
    public Question getQuestion(String id) {
        Question question = questionRepository.findOne(id);
        if (question == null) {
            log.info(String.format("Question with id '%s' doesn't exists in database.", id));
            return null;
        }
        log.info(String.format("Question '%s' has been received from database", id));
        return question;
    }

    @Override
    public void addQuestion(Question question) {
        log.info(String.format("Question with '%s' has been added. ID:: '%s'", question.getQuestionString(), question.getId()));
        questionRepository.save(question);
    }

    @Override
    public void updateQuestion(Question question) {
        if (question == null) {
            log.info(String.format("Question '%s' doesn't exists in database.", question));
        } else {
            log.info(String.format("Question:: '%s' has been updated", question));
            questionRepository.save(question);
        }
    }

    @Override
    public void removeQuestion(String id) {
        Question question = questionRepository.findOne(id);
        if (question == null) {
            log.info(String.format("Question with id '%s' doesn't exists in database.", id));
        } else {
            log.info(String.format("Question with id '%s' has been deleted from database", id));
            questionRepository.delete(question);
        }

    }
}
