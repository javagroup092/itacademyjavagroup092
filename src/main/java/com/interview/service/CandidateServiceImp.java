package com.interview.service;

import com.interview.model.Candidate;
import com.interview.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Calendar;
import java.util.List;

@Service
public class CandidateServiceImp implements CandidateService{
    @Autowired
    CandidateRepository candidateRepository;

    @Override
    public void addCandidate(Candidate candidate) {
        candidate.setId(null);

        candidateRepository.save(candidate);
    }

    @Override
    public Candidate getCandidate(String id) {
        return  candidateRepository.findOne(id);
    }

    @Override
    public void updateCandidate(Candidate candidate){
        candidateRepository.save(candidate);
    }

    @Override
    public void removeCandidate(String id) {
        candidateRepository.delete(id);
    }

    @Override
    public List<Candidate> candidateList() {
        return candidateRepository.findAll();
    }

    @Override
    public void clearList() {
        candidateRepository.deleteAll();
    }
}