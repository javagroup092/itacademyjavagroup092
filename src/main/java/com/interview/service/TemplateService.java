package com.interview.service;

import com.interview.model.Question;
import com.interview.model.Template;

import java.util.List;

/**
 * @author Anton Kruglikov.
 */
public interface TemplateService {
    List<Template> getAllTemplates();
    Template getTemplate(String id);
    void addTemplate(Template template);
    void updateTemplate(Template template);
    void removeTemplate(String id);

    List<Question> getQuestionsFromTemplate(String id);
    void addQuestionToTemplate(String templateId, Question question);
    void deleteQuestionFromTemplate(String templateId, String questionId);
}
