package com.interview.service;

import com.interview.model.Candidate;
import com.interview.model.Group;

import java.util.Calendar;
import java.util.List;

/**
 * @author Yegor Gulimov
 */
public interface GroupService {
    List<Group> list();
    Group get(String id);
    Group getByName(String name);
    Group add(Group group);
    boolean remove(String id);
    Group update(String id, Group updatedGroup);

    List<Candidate> getGroupCandidatesByDate(String id, Calendar date);
    List<Calendar> getGroupDates(String id);
}
