package com.interview.controller.web;

import com.interview.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Yegor Gulimov
 */

@Controller
@RequestMapping("/web/groups")
public class MainController {

    @Autowired
    private GroupService groupService;

    @RequestMapping()
    public ModelAndView getGroups() {
        return new ModelAndView("interviewer/groups");
    }

    @RequestMapping("/{id}")
    public ModelAndView getGroup(@PathVariable("id") String id) {
        ModelAndView modelAndView = new ModelAndView("interviewer/group");
        modelAndView.addObject("id", id);
        return modelAndView;
    }
    @RequestMapping("/{id}/groupReport")
    public ModelAndView getGroupReport(@PathVariable("id") String id) {
        ModelAndView modelAndView = new ModelAndView("interviewer/groupReport");
        modelAndView.addObject("id", id);
        return modelAndView;
    }
}
