package com.interview.controller.web;

import com.interview.model.dto.Day;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Yegor Gulimov
 */
@Deprecated
@Controller
@RequestMapping("/web/day")
public class DayControllerWeb {

    @RequestMapping()
    public ModelAndView day(@ModelAttribute("day") Day day) {
        return new ModelAndView("day/day");
    }
}
