package com.interview.controller.web;

import com.interview.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.websocket.server.PathParam;
import java.util.GregorianCalendar;

/**
 * Created by SD on 24.03.2016.
 */
@Controller
@RequestMapping("/web")
public class ProfileController {

    @RequestMapping("/candidate/{id}")
    public ModelAndView candidateProfile(@PathVariable("id") String id) {
        return new ModelAndView( "candidate/candidateProfile", "id", id);
    }

    @RequestMapping("/profile")
    public ModelAndView interviewerProfile() {
        return new ModelAndView("interviewer/interviewerProfile");
    }
}