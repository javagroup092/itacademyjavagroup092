package com.interview.controller.web;

import com.interview.model.dto.Day;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Yegor Gulimov
 */

@Deprecated
@Controller
@RequestMapping("/web/session")
public class SessionControllerWeb {

    @RequestMapping()
    public ModelAndView session() {
        return new ModelAndView("day/session");
    }

    @RequestMapping("/day")
    public ModelAndView sessionDay() {
        return new ModelAndView("day/sessionDay");
    }

}
