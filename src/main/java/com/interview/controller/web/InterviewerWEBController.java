package com.interview.controller.web;

import com.interview.model.Interviewer;
import com.interview.service.InterviewerService;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author NSS
 */
@Controller
@RequestMapping(value = "/web/admin")
public class InterviewerWEBController {
    
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private InterviewerService interviewerService;
    
    @RequestMapping(method = RequestMethod.GET)
    public String signin(Model uiModel) {
        return "signin";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model uiModel) {
        return "login";
    }
    
    @RequestMapping(value = "/interviewer", method = RequestMethod.GET)
    public String list(Model uiModel) {
        List<Interviewer> interviewers = interviewerService.getInterviewers();
        uiModel.addAttribute("interviewers", interviewers);
        return "interviewer/list";
    }

    @RequestMapping(value = "/interviewer/{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id") String id, Model uiModel) {
        Interviewer interviewer = interviewerService.getInterviewer(id);
        uiModel.addAttribute("interviewer", interviewer);
        return "interviewer/show";
    }

    @RequestMapping(value = "/interviewer/{id}", params = "form", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") String id, Model uiModel) {
        uiModel.addAttribute("interviewer", interviewerService.getInterviewer(id));
        return "interviewer/edit";
    }

    @RequestMapping(value = "/interviewer", params = "form", method = RequestMethod.GET)
    public String createForm(Model uiModel) {
        Interviewer interviewer = new Interviewer();
        uiModel.addAttribute("interviewer", interviewer);
        return "interviewer/edit";
    }

    @RequestMapping(value = "/interviewer/save", method = RequestMethod.POST)
    public String updateOrInsert(@ModelAttribute("interviewer") Interviewer interviewer) {

        if (interviewer.getId() == null) {
            interviewerService.updateInterviewer(interviewer);
        } else {
            interviewerService.updateInterviewer(interviewer);
        }

        return "redirect:/web/admin/interviewer";
    }

    @RequestMapping(value = "/interviewer/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") String id, Model uiModel) {
        interviewerService.removeInterviewer(id);
        return "redirect:/web/admin/interviewer";
    }
//------------------------------------------------------------------------------ 

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accesssDenied(Model uiModel) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            uiModel.addAttribute("username", userDetail.getUsername());
        }
        return "403";
    }

    @RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
    public String loginerror(Model uiModel) {
        uiModel.addAttribute("error", "true");
        return "denied";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "logout";
    }
}
