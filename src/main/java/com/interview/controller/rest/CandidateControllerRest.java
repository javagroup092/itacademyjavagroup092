package com.interview.controller.rest;

import com.interview.model.Candidate;
import com.interview.service.CandidateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class CandidateControllerRest {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CandidateService candidateService;

    @RequestMapping(value = "/candidates/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addCandidate(@RequestBody Candidate candidate){
        try {
            candidateService.addCandidate(candidate);
            log.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e){
            log.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/candidates/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Candidate> getCandidate(@PathVariable("id") String id){
        Candidate candidate = candidateService.getCandidate(id);

        if(candidate != null){
            log.info("HttpStatus: OK");
            return new ResponseEntity<>(candidate,HttpStatus.OK);
        }
        log.error("HttpStatus: NOT_FOUND");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/candidates/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> removeCandidate(@PathVariable("id") String id){
        try {
            candidateService.removeCandidate(id);
            log.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e){
            log.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/candidates/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateCandidate(@RequestBody Candidate candidate, @PathVariable ("id") String id){
        candidate.setId(id);

        try {
            candidateService.updateCandidate(candidate);
            log.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e){
            log.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/candidates", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Candidate>> candidateList(){
        List<Candidate> candidates = candidateService.candidateList();

        if (candidates.isEmpty()){
            log.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        log.info("HttpStatus: OK");
        return new ResponseEntity<>(candidates,HttpStatus.OK);
    }

    @RequestMapping(value = "/candidates/clear", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> clearCandidate(){
        candidateService.clearList();
        log.info("HttpStatus: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
