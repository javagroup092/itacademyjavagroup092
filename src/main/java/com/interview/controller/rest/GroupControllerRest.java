package com.interview.controller.rest;

import com.interview.model.Candidate;
import com.interview.model.Group;
import com.interview.model.Interviewer;
import com.interview.model.Template;
import com.interview.service.CandidateService;
import com.interview.service.GroupService;
import com.interview.service.InterviewerService;
import com.interview.service.TemplateService;
import com.interview.util.GroupTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author Yegor Gulimov
 */

@RestController
@RequestMapping("/rest")
public class GroupControllerRest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HttpServletRequest request;

    //Services

    @Autowired
    private GroupService groupService;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private InterviewerService interviewerService;

    @Autowired
    private TemplateService templateService;



    //Basic CRUD

    @RequestMapping("/groups")
    public ResponseEntity<List<Group>> getGroups() {
        logRequestInfo(request);
        List<Group> groups = groupService.list();
        if (groups == null) {
            log.error("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (groups.isEmpty()) {
            log.info("HTTP Status: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        log.info("HTTP Status: OK");
        return new ResponseEntity<>(groups, HttpStatus.OK);
    }

    @RequestMapping("/groups/{id}")
    public ResponseEntity<Group> getGroup(@PathVariable("id") String id) {
        logRequestInfo(request);
        Group receivedGroup = groupService.get(id);
        if (receivedGroup == null) {
            log.info("HTTP Status: NOT_FOUND");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(receivedGroup, HttpStatus.OK);
    }

    @RequestMapping(value = "/groups/", method = RequestMethod.POST)
    public ResponseEntity<Group> addGroup(@RequestBody Group group,
                                          UriComponentsBuilder ucb) {
        logRequestInfo(request);
        Group receivedGroup = groupService.add(group);
        if (receivedGroup == null) {
            log.info("HTTP Status: CONFLICT");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/rest/groups/{id}").
                buildAndExpand(receivedGroup.getId()).toUri());

        log.info("HTTP Status: CREATED");
        return new ResponseEntity<>(receivedGroup, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/groups/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeGroup(@PathVariable("id") String id) {
        logRequestInfo(request);
        if (groupService.remove(id)) {
            log.info("HTTP Status: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        }

        log.info("HTTP Status: NOT_FOUND");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/groups/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Group> updateGroup(@PathVariable("id") String id,
                                             @RequestBody Group updatedGroup) {
        logRequestInfo(request);
        Group receivedGroup = groupService.update(id, updatedGroup);
        if (receivedGroup == null) {
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(receivedGroup, HttpStatus.OK);
    }


    //Improved invocations for interacting with entity Candidate

    @RequestMapping(value = "/groups/{groupId}/addCandidate/{candidateId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> addCandidateToGroup(@PathVariable("groupId") String groupId,
                                    @PathVariable("candidateId") String candidateId) {
        logRequestInfo(request);
        Group receivedGroup = groupService.get(groupId);
        Candidate receivedCandidate = candidateService.getCandidate(candidateId);
        if (receivedGroup == null || receivedCandidate == null) {
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Candidate> candidates = receivedGroup.getCandidates();
        for (Candidate candidate : candidates) {
            if (candidate.getId().equals(receivedCandidate.getId())) {
                log.info(String.format(
                        "Group '%s' already has candidate '%s'. Duplicates aren't allowed",
                        receivedGroup, receivedCandidate));
                log.info("HTTP Status: CONFLICT");
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        }
        receivedGroup.getCandidates().add(receivedCandidate);
        groupService.update(receivedGroup.getId(), receivedGroup);

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/groups/{groupId}/removeCandidate/{candidateId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> removeCandidateFromGroup(@PathVariable("groupId") String groupId,
                                                         @PathVariable("candidateId") String candidateId) {
        logRequestInfo(request);
        Group receivedGroup = groupService.get(groupId);
        Candidate receivedCandidate = candidateService.getCandidate(candidateId);
        if (receivedGroup == null || receivedCandidate == null) {
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Candidate> candidates = receivedGroup.getCandidates();
        int candidateIdx = -1;
        for (int i = 0; i < candidates.size(); i++) {
            Candidate candidate = candidates.get(i);
            if (candidate.getId().equals(receivedCandidate.getId())) {
                candidateIdx = i;
                break;
            }
        }

        if (candidateIdx == -1) {
            log.info(String.format(
                    "Group '%s' doesn't contain candidate '%s'. Nothing to remove",
                    receivedGroup, receivedCandidate));
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        receivedGroup.getCandidates().remove(receivedGroup.getCandidates().get(candidateIdx));
        groupService.update(receivedGroup.getId(), receivedGroup);

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }


    //Improved invocations for interacting with entity Interviewer

    @RequestMapping(value = "/groups/{groupId}/addInterviewer/{interviewerId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> addInterviewerToGroup(@PathVariable("groupId") String groupId,
                                                      @PathVariable("interviewerId") String interviewerId) {
        logRequestInfo(request);
        Group receivedGroup = groupService.get(groupId);
        Interviewer receivedInterviewer = interviewerService.getInterviewer(interviewerId);

        if (receivedGroup == null || receivedInterviewer == null) {
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Interviewer> interviewers = receivedGroup.getInterviewers();
        for (Interviewer interviewer : interviewers) {
            if (interviewer.getId().equals(receivedInterviewer.getId())) {
                log.info(String.format(
                        "Group '%s' already contains interviewer '%s'. Duplicates aren't allowed",
                        receivedGroup, receivedInterviewer));
                log.info("HTTP Status: CONFLICT");
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        }
        receivedGroup.getInterviewers().add(receivedInterviewer);
        groupService.update(receivedGroup.getId(), receivedGroup);

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/groups/{groupId}/removeInterviewer/{interviewerId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> removeInterviewerFromGroup(@PathVariable("groupId") String groupId,
                                                           @PathVariable("interviewerId") String interviewerId) {
        logRequestInfo(request);
        Group receivedGroup = groupService.get(groupId);
        Interviewer receivedInterviewer = interviewerService.getInterviewer(interviewerId);
        if (receivedGroup == null || receivedInterviewer == null) {
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Interviewer> interviewers = receivedGroup.getInterviewers();
        int interviewerIdx = -1;
        for (int i = 0; i < interviewers.size(); i++) {
            Interviewer interviewer = interviewers.get(i);
            if (interviewer.getId().equals(receivedInterviewer.getId())) {
                interviewerIdx = i;
                break;
            }
        }
        if (interviewerIdx == -1) {
            log.info(String.format(
                    "Group '%s' doesn't contain interviewer '%s'. Nothing to remove",
                    receivedGroup, receivedInterviewer));
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        receivedGroup.getInterviewers().remove(receivedGroup.getInterviewers().get(interviewerIdx));
        groupService.update(receivedGroup.getId(), receivedGroup);

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //Improved invocations for interacting with entity Template

    @RequestMapping(value = "/groups/{groupId}/setTemplate/{templateId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> setTemplateForGroup(@PathVariable("groupId") String groupId,
                                                    @PathVariable("templateId") String templateId) {
        logRequestInfo(request);
        Group receivedGroup = groupService.get(groupId);
        Template receivedTemplate = templateService.getTemplate(templateId);
        if (receivedGroup == null || receivedTemplate == null) {
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        receivedGroup.setTemplate(receivedTemplate);
        groupService.update(receivedGroup.getId(), receivedGroup);

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/groups/{groupId}/resetTemplate", method = RequestMethod.PUT)
    public ResponseEntity<Void> resetTemplateForGroup(@PathVariable("groupId") String groupId) {
        logRequestInfo(request);
        Group receivedGroup = groupService.get(groupId);
        if (receivedGroup == null) {
            log.info("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        receivedGroup.setTemplate(null);
        groupService.update(receivedGroup.getId(), receivedGroup);

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //for correct rests

    @RequestMapping("/groups/id")
    public ResponseEntity<Map<String, String>> getGroupIds() {
        logRequestInfo(request);
        List<Group> groups = groupService.list();
        if (groups == null) {
            log.error("HTTP Status: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (groups.isEmpty()) {
            log.info("HTTP Status: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        Map<String, String> groupsMap = new TreeMap<>();
        for (Group group : groups) {
            groupsMap.put(group.getId(), group.getName());
        }

        log.info("HTTP Status: OK");
        return new ResponseEntity<>(groupsMap, HttpStatus.OK);
    }

    @RequestMapping("/groups/{id}/candidatesByDate/{date}")
    public ResponseEntity<List<Candidate>> getCandidatesByDate(@PathVariable("id") String id,
                                                               @PathVariable("date") String date) {
        logRequestInfo(request);
        Group group = groupService.get(id);
        if (group == null) {
            log.info("HTTP Status: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        List<Candidate> candidates = groupService.getGroupCandidatesByDate(id, GroupTool.parseStringToDate(date));
        return new ResponseEntity<>(candidates, HttpStatus.OK);
    }

    @RequestMapping("/groups/{id}/dates")
    public ResponseEntity<List<Calendar>> getGroupDates(@PathVariable("id") String id) {
        logRequestInfo(request);
        Group group = groupService.get(id);
        if (group == null) {
            log.info("HTTP Status: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        List<Calendar> dates = groupService.getGroupDates(id);
        return new ResponseEntity<List<Calendar>>(dates, HttpStatus.OK);
    }




    private void logRequestInfo(HttpServletRequest request) {
        Enumeration headers = request.getHeaderNames();
        StringBuilder builder = new StringBuilder("\n");
        while (headers.hasMoreElements()) {
            String key = (String) headers.nextElement();
            builder.append("- ").
                    append(key).
                    append(": ").
                    append(request.getHeader(key)).
                    append("\n");
        }
        log.info(String.format(
                        "--------------------\n" +
                        "Request URL: %s\n" +
                        "Request Method: %s\n" +
                        "Request Headers: %s",
                request.getRequestURL(),
                request.getMethod(),
                builder.toString()));
    }
}
