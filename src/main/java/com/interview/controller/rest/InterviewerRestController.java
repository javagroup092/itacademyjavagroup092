package com.interview.controller.rest;

import com.interview.model.Interviewer;
import com.interview.service.InterviewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class InterviewerRestController {

    @Autowired
    private InterviewerService service;

    @RequestMapping(value = "/interviewers", method = RequestMethod.GET)
    public ResponseEntity getAll() {
        final List<Interviewer> interviewers = service.getInterviewers();
        if (interviewers == null) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<>(interviewers, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/interviewers/{id}", method = RequestMethod.GET)
    public ResponseEntity get(@PathVariable String id) {
        final Interviewer interviewer = service.getInterviewer(id);
        if (interviewer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(interviewer, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/interviewers/add", method = RequestMethod.POST)
    public ResponseEntity add(@RequestBody Interviewer interviewer) {
        try {
            service.addInterviewer(interviewer);
            return new ResponseEntity<>(interviewer, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/interviewers/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody Interviewer interviewer, @PathVariable("id") String id) {
        interviewer.setId(id);
        interviewer = service.updateInterviewer(interviewer);
        if (interviewer == null) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<>(interviewer, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/interviewers/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") String id) {
        try {
            service.removeInterviewer(id);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

}
