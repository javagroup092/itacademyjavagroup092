package com.interview.controller.rest;

import com.interview.model.Interview;
import com.interview.service.InterviewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Simple rest controller which is responsible for C.R.U.D.
 * and for interview manipulating operations:
 *      GET    - /rest/interviews    - get all interviews
 *      GET    - /rest/interviews/id - get interview with specified id
 *      POST   - /rest/interviews    - add interview
 *      PUT   - /rest/interviews/id  - update interview with specified id
 *      DELETE - /rest/interviews/id - delete interview from the repository
 *
 * Simple way to create interview with specified interviewer is to set interviewer id
 * in interviewer field
 *
 * @author Artem Baranovskiy
 */
@RestController
@RequestMapping("/rest/interviews")
public class InterviewRestController {

    private static final Logger LOG = LoggerFactory.getLogger(InterviewRestController.class);

    @Autowired
    private InterviewService interviewService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAll() {
        final List<Interview> interviews = interviewService.getInterviews();
        if (interviews == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } else {
            LOG.info("Fetching all interviews: OK");
            return new ResponseEntity<>(interviews, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity read(@PathVariable("id") String id) {
        final Interview interview = interviewService.getInterview(id);
        if (interview == null) {
            LOG.error("Fetching {} interview: NO_CONTENT", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            LOG.info("Fetching {} interview: OK", id);
            return new ResponseEntity<>(interview, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody @Valid Interview interview, BindingResult result) {
        if (result.hasErrors()) {
            LOG.error("Creating interview: {}, CONFLICT", result.getFieldError().getDefaultMessage());
            return new ResponseEntity<>(result.getFieldError().getDefaultMessage(), HttpStatus.CONFLICT);
        }
        if (interviewService.addInterview(interview) != null) {
            LOG.info("Creating interview: OK");
            return new ResponseEntity(HttpStatus.OK);
        } else {
            LOG.error("Creating interview: CONFLICT");
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody @Valid Interview interview,
                                         BindingResult result,
                                         @PathVariable("id") String id) {
        if (result.hasErrors()) {
            LOG.error("Updating interview: {}, CONFLICT", result.getFieldError().getDefaultMessage());
            return new ResponseEntity<>(result.getFieldError().getDefaultMessage(), HttpStatus.CONFLICT);
        }
        interview.setId(id);
        if (interviewService.getInterview(id) != null
                && interviewService.updateInterview(interview) != null ) {
            LOG.info("Updating interview: OK");
            return new ResponseEntity(HttpStatus.OK);
        } else {
            LOG.error("Updating interview: CONFLICT");
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") String id) {
        try {
            interviewService.deleteInterview(id);
            LOG.info("Deleting interview: OK");
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex) {
            LOG.error("Deleting interview: CONFLICT");
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }

}

