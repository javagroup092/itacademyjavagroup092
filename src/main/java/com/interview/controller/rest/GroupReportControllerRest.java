package com.interview.controller.rest;

import com.interview.model.Candidate;
import com.interview.model.Group;
import com.interview.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Artem Pozdeev on 24.03.2016.
 */

@RestController
@RequestMapping("/rest")
public class GroupReportControllerRest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HttpServletRequest request;

    //Services

    @Autowired
    private GroupService groupService;

    @RequestMapping("/groups/{id}/groupReport")
    public ResponseEntity<List<Candidate>> getCandidates(@PathVariable("id") String id) {
        Group group = groupService.get(id);
        if (group == null) {
            log.info("HTTP Status: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        List<Candidate> candidates = group.getCandidates();
        return new ResponseEntity<>(candidates, HttpStatus.OK);
    }
}
