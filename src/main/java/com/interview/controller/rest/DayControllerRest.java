package com.interview.controller.rest;

import com.interview.model.dto.Day;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

/**
 * @author Yegor Gulimov
 */
@Deprecated
@RestController
@RequestMapping("/rest/day")
public class DayControllerRest {

    @RequestMapping()
    public ResponseEntity<Day> getDay(@RequestParam("date") String date,
                                      @RequestParam("direction") String direction) {
        Calendar checkedDate = Day.parseStringToCalendar(date);
        String checkedDirection = Day.parseStringToDirection(direction);
        if (checkedDate == null || checkedDirection == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Day day = new Day(checkedDate, checkedDirection);
        if (day.getDate() == null || day.getDirection() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(day, HttpStatus.OK);
    }
}
