package com.interview.controller.rest;

import com.interview.model.Question;
import com.interview.model.Template;
import com.interview.service.QuestionService;
import com.interview.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Anton Kruglikov.
 */
@RestController
@RequestMapping("/rest/templates")
public class TemplateControllerRest {

    @Autowired
    TemplateService templateService;

    @Autowired
    QuestionService questionService;

    @RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Template>> templateList(){
        List<Template> templates = templateService.getAllTemplates();

        if (templates.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(templates, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Template> getTemplate(@PathVariable("id") String id){
        Template template = templateService.getTemplate(id);

        if(template != null){
            return new ResponseEntity<>(template, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addTemplate(@RequestBody Template template){

        try {

            List<Question> questionList = template.getQuestions();
            for (Question question : questionList) {
                questionService.addQuestion(question);
            }

            templateService.addTemplate(template);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateTemplate(@RequestBody Template template, @PathVariable ("id") String id){
        template.setId(id);

        try {

            List<Question> questionList = template.getQuestions();
            for (Question question : questionList) {
                questionService.addQuestion(question);
            }

            templateService.updateTemplate(template);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> removeTemplate(@PathVariable("id") String id){
        try {
            templateService.removeTemplate(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/questions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Question>> questionsFromTemplate(@PathVariable("id") String id){
        List<Question> questions = templateService.getQuestionsFromTemplate(id);

        if (questions.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(questions, HttpStatus.OK);
    }

    @RequestMapping(value = "/questions/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addQuestionToTemplate(@RequestBody Template template, @RequestBody Question question){

        try {
            templateService.addQuestionToTemplate(template.getId(), question);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> removeQuestionFromTemplate(@RequestBody Template template, @PathVariable("id") String id){
        try {
            templateService.deleteQuestionFromTemplate(template.getId(), id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}