package com.interview.controller.rest;

import com.interview.model.dto.Session;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Yegor Gulimov
 */

@Deprecated
@RestController
@RequestMapping("/rest/session")
public class SessionControllerRest {

    @RequestMapping()
    public ResponseEntity<Session> session() {
        return new ResponseEntity<>(new Session(), HttpStatus.OK);
    }
}
