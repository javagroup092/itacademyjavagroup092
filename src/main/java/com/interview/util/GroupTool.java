package com.interview.util;

import com.interview.model.Candidate;

import java.text.ParseException;
import java.util.*;

/**
 * @author Yegor Gulimov
 */
public class GroupTool {
    public static boolean compareDates(Calendar cal1, Calendar cal2) {
        return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE));
    }

    public static Set<Calendar> getDates(List<Candidate> candidates) {
        if (candidates == null || candidates.isEmpty()) {
            return null;
        }

        Set<Calendar> dates = new TreeSet<>();


        for (Candidate candidate : candidates) {
            dates.add(cutDate(candidate.getDate()));
        }

        return dates;
    }

    public static Calendar cutDate(Calendar date) {
        return new GregorianCalendar(
                date.get(Calendar.YEAR), date.get(Calendar.MONTH) ,date.get(Calendar.DATE)
        );
    }

    public static Calendar parseStringToDate(String strDate) {
        long longDate = 0L;
        try {
            longDate = Long.parseLong(strDate);
        } catch (NumberFormatException e) {
            return null;
        }
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date(longDate));
        return calendar;
    }
}
