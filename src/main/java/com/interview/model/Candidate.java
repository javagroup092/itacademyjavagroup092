package com.interview.model;

import org.springframework.data.annotation.Id;

import java.io.File;
import java.io.Serializable;
import java.util.Calendar;

public class Candidate implements Serializable {
    @Id
    private String id;
    private String surname;
    private String name;
    private String patronymic;
    private Calendar date;
    private EnglishLevel englishLevel;
    private String phoneNumber;
    private String email;
    private String skype;
    private Group group;

    public Candidate() {
    }

    public Candidate(String surname, String name, String patronymic,
                     File cv, Calendar date, EnglishLevel englishLevel,
                     String phoneNumber, String email, String skype, Group group) {

        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.date = date;
        this.englishLevel = englishLevel;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.skype = skype;
        this.group = group;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }


    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setEnglishLevel(EnglishLevel englishLevel) {
        this.englishLevel = englishLevel;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candidate candidate = (Candidate) o;

        if (id != null ? !id.equals(candidate.id) : candidate.id != null) return false;
        if (name != null ? !name.equals(candidate.name) : candidate.name != null) return false;
        if (patronymic != null ? !patronymic.equals(candidate.patronymic) : candidate.patronymic != null) return false;
        if (surname != null ? !surname.equals(candidate.surname) : candidate.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", englishLevel='" + englishLevel + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", skype='" + skype + '\'' +
                ", group='" + group + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Calendar getDate() {
        return date;
    }

    public EnglishLevel getEnglishLevel() {
        return englishLevel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getSkype() {
        return skype;
    }

    public Group getGroup() {
        return group;
    }

    public enum EnglishLevel {
        INTERMEDIATE, UPPERINTERMEDIATE;
    }

    public enum Group {
        JAVA, PYTON;
    }
}