package com.interview.model;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Anton Kruglikov.
 */
@Document(collection = "interviewQuestions")
@TypeAlias("interviewQuestion")
public class InterviewQuestion extends Question {

    private double finalQuestionvalue;
    private boolean skipped;
    private boolean failed;

    public InterviewQuestion() {

    }

    @PersistenceConstructor
    public InterviewQuestion(double finalQuestionvalue, boolean skipped, boolean failed) {
        super();
        this.finalQuestionvalue = finalQuestionvalue;
        this.skipped = skipped;
        this.failed = failed;
    }

    public void setFinalQuestionvalue(double value) {
        this.finalQuestionvalue = value;
    }

    public double getFinalQuestionvalue() {
        return finalQuestionvalue;
    }

    public void setSkipped(boolean skipped) {
        this.skipped = skipped;
    }

    public boolean isSkipped() {
        return skipped;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public boolean isFailed() {
        return failed;
    }

    @Override
    public String toString() {
        return "[" + getQuestionString()
                + ", " + getMaxQuestionValue()
                + ", " + getFinalQuestionvalue()
                + ", " + isFailed()
                + ", " + isSkipped()
                + "]";
    }

}
