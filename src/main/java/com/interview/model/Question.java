package com.interview.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author Anton Kruglikov.
 */
@Document(collection="questions")
@TypeAlias("question")
public class Question implements Serializable {

    @Id
    private String id;

    private String questionString;
    private double maxQuestionValue;

    public Question() {}

    @PersistenceConstructor
    public Question(String questionString, double maxValue) {
        this.questionString = questionString;
        this.maxQuestionValue = maxValue;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setQuestionString(String questionString) {
        this.questionString = questionString;
    }

    public String getQuestionString() {
        return questionString;
    }

    public void setMaxQuestionValue(double maxValue) {
        this.maxQuestionValue = maxValue;
    }

    public double getMaxQuestionValue() {
        return maxQuestionValue;
    }

    @Override
    public String toString() {
        return "[" + getQuestionString()
                + ", " + getMaxQuestionValue()
                + "]";
    }
}