package com.interview.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

/**
 * @author Anton Kruglikov.
 */
@Document(collection="templates")
@TypeAlias("template")
public class Template implements Serializable {

    @Id
    private String id;
    private String name;
    private boolean favourite;
    private String direction;

    @DBRef
    private List<Question> questions;

    public Template() {}

    @PersistenceConstructor
    public Template(String name, List<Question> questions, boolean favourite, String direction) {
        this.name = name;
        this.questions = questions;
        this.favourite = favourite;
        this.direction = direction;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setQuestion(Question question) {
        questions.add(question);
    }

    @Override
    public String toString() {
        return "[" + getName()
                + ", " + getQuestions()
                + ", " + isFavourite()
                + ", " + getDirection()
                + "]";
    }


}