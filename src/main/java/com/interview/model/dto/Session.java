package com.interview.model.dto;

import com.interview.model.Candidate;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Yegor Gulimov
 */
@Deprecated
public class Session {

    private List<Day> days = new ArrayList<>();
    private List<DumbCandidate> candidates = new ArrayList<>();
    private List<Calendar> dates = new ArrayList<>();
    private List<String> directions = new ArrayList<>();

    public Session() {
        candidates = Day.simulationGetCandidatesFromService();
        dates = findDates();
        directions = findDirections();
        days = findDays();
    }

    public List<Calendar> findDates() {
        List<Calendar> dates = new ArrayList<>();
        for (DumbCandidate candidate : candidates) {
            Calendar calendar = candidate.getCalendar();

            boolean isNewDate = true;
            for (Calendar date : dates) {
                if (Day.compareDates(calendar, date)) {
                    isNewDate = false;
                    break;
                }
            }
            if (isNewDate) {
                dates.add(calendar);
            }
        }
        Collections.sort(dates);

        return dates;
    }

    public List<String> findDirections() {
        List<String> directions = new ArrayList<>();
        for (DumbCandidate candidate : candidates) {
            String direction = candidate.getDirection();

            boolean isNewDirection = true;
            for (String d : directions) {
                if (Day.compareDirections(d, direction)) {
                    isNewDirection = false;
                    break;
                }
            }
            if (isNewDirection) {
                directions.add(direction);
            }
        }
        Collections.sort(directions);

        return directions;
    }

    public List<Day> findDays() {
        List<Day> dayList = new ArrayList<>();
        for (Calendar c : dates) {
            for (String d : directions) {
                dayList.add(new Day(c, d));
            }
        }
        Iterator<Day> iterator = dayList.iterator();
        while (iterator.hasNext()) {
            Day d = iterator.next();
            if (d.getCandidates().isEmpty()) {
                iterator.remove();
            }
        }
        return dayList;
    }

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    public List<DumbCandidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<DumbCandidate> candidates) {
        this.candidates = candidates;
    }

    public List<Calendar> getDates() {
        return dates;
    }

    public void setDates(List<Calendar> dates) {
        this.dates = dates;
    }

    public List<String> getDirections() {
        return directions;
    }

    public void setDirections(List<String> directions) {
        this.directions = directions;
    }


/*
    public static void main(String[] args) {
        Session session = new Session();

        List<Calendar> dates = session.getDates();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy", Locale.ENGLISH);
        System.out.println("-- DATES --");
        for (Calendar date : dates) {
            System.out.println(sdf.format(date.getTime()));
        }

        List<String> directions = session.getDirections();
        System.out.println("\n-- DIRECTIONS --");
        for (String direction : directions) {
            System.out.println(direction);
        }

        List<Day> days = session.getDays();
        System.out.println("\n-- DAYS --");
        for (Day day : days) {
            System.out.println(day);
        }
    }
    */
}
