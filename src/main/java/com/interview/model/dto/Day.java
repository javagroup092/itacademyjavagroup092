package com.interview.model.dto;

import com.interview.model.Candidate;
import com.interview.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Yegor Gulimov
 */

@Deprecated
public class Day {
    //Dumb realization
    private static List<DumbCandidate> dumbCandidates = new ArrayList<>();

    static {
        dumbCandidates.add(new DumbCandidate("john",    new GregorianCalendar(2016, 3, 14, 11, 30), "java"));
        dumbCandidates.add(new DumbCandidate("paul",    new GregorianCalendar(2016, 3, 14, 10, 45), "java"));
        dumbCandidates.add(new DumbCandidate("george",  new GregorianCalendar(2016, 3, 14, 11, 0), "java"));
        dumbCandidates.add(new DumbCandidate("richard", new GregorianCalendar(2016, 3, 15, 13, 0), "java"));
        dumbCandidates.add(new DumbCandidate("steven",  new GregorianCalendar(2016, 3, 15, 13, 15), "java"));
        dumbCandidates.add(new DumbCandidate("mike",    new GregorianCalendar(2016, 3, 15, 13, 30), "python"));
        dumbCandidates.add(new DumbCandidate("travis",  new GregorianCalendar(2016, 3, 15, 13,45), "java"));
        dumbCandidates.add(new DumbCandidate("joshua",  new GregorianCalendar(2016, 3, 16, 10, 0), "java"));
        dumbCandidates.add(new DumbCandidate("alice",   new GregorianCalendar(2016, 3, 14, 10, 0), "python"));
        dumbCandidates.add(new DumbCandidate("jane",    new GregorianCalendar(2016, 3, 15, 11, 30), "python"));
        dumbCandidates.add(new DumbCandidate("ashley",  new GregorianCalendar(2016, 3, 16, 13, 30), "java"));
    }

    //will be deleted
    public static List<DumbCandidate> simulationGetCandidatesFromService() {
        return dumbCandidates;
    }

    private Calendar date;
    private String direction;//replace with Direction entity

    List<DumbCandidate> candidates = new ArrayList<>();

    public Day() {}

    public Day(Calendar date, String direction) {
        this.date = date;
        this.direction = direction;
        findSuitableCandidates();
    }

    public void findSuitableCandidates() {
        //replace with actual candidates from Candidates Collection in DB
        candidates.clear();
        for (DumbCandidate dumbCandidate : dumbCandidates) {
            if (compareDates(dumbCandidate.getCalendar(), this.date) &&
                    compareDirections(dumbCandidate.getDirection(), this.direction)) {
                candidates.add(dumbCandidate);
            }
        }
        Collections.sort(candidates);
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public List<DumbCandidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<DumbCandidate> candidates) {
        this.candidates = candidates;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy", Locale.ENGLISH);
        String candidatesStr = "";
        for (DumbCandidate candidate : candidates) {
            candidatesStr += "\n - " + candidate.getName() + " : " +
                    candidate.getCalendar().get(Calendar.HOUR) + "-" + candidate.getCalendar().get(Calendar.MINUTE);
        }
        candidatesStr += "\n";

        return direction + " - " + sdf.format(date.getTime()) + candidatesStr;
    }

    //test
    /*
    public static void main(String[] args) {
        Day day = new Day(new GregorianCalendar(2016, 3, 14), "java");
        System.err.println("Candidates: ");
        for (DumbCandidate candidate : day.getCandidates()) {
            System.err.println(candidate);
        }
        System.out.println(parseStringToCalendar(null));
    }
    */

    public static boolean compareDates(Calendar cal1, Calendar cal2) {
        return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE));
    }

    public static boolean compareDirections(String dir1, String dir2) {
        return dir1.equals(dir2);
    }

    public static Calendar parseStringToCalendar(String stringDate) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy", Locale.ENGLISH);
        try {
            cal.setTime(sdf.parse(stringDate));
        } catch (NullPointerException | ParseException e) {
            return null;
        }
        return cal;
    }

    //method will be modified after actual Direction class will have been created
    public static String parseStringToDirection(String stringDirection) {
        return stringDirection;
    }


}
//I'll delete this dummy class when actual Candidate will have been completed
@Deprecated
class DumbCandidate implements Comparable<DumbCandidate> {
    private String name;
    private Calendar calendar;
    private String direction;

    public DumbCandidate(String name, Calendar calendar, String direction) {
        this.name = name;
        this.calendar = calendar;
        this.direction = direction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        return name + " : " + sdf.format(calendar.getTime()) + " : " + direction;
    }

    @Override
    public int compareTo(DumbCandidate o) {
        return calendar.compareTo(o.calendar);
    }
}
