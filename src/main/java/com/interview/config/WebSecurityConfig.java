package com.interview.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired
    private PersistentTokenRepository repository;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("user").password("password").roles("ADMIN");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/styles/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.exceptionHandling()
                .accessDeniedPage("/web/admin/403")
                .and()
                .authorizeRequests()
                .antMatchers( "/web/admin" ).permitAll()
                .antMatchers("/web/admin/interviewer/**").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/web/admin/login")
                .defaultSuccessUrl("/web/admin/interviewer")
                .permitAll()
                .failureUrl("/web/admin/accessdenied")
                .and()
                .logout()
                .permitAll()
                .and().rememberMe().tokenRepository(repository).tokenValiditySeconds(1209600)
                .and()
                .csrf().disable();
        
    }
}