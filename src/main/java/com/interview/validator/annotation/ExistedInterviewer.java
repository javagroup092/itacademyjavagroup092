package com.interview.validator.annotation;


import com.interview.validator.InterviewerValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 *
 * @author Artem Baranovskiy
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = InterviewerValidator.class)
public @interface ExistedInterviewer {

    String message() default "Interviewer with such id is not exists";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
