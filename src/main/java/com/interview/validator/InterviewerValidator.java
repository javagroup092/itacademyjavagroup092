package com.interview.validator;

import com.interview.repository.InterviewerRepository;
import com.interview.validator.annotation.ExistedInterviewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author Artem Baranovskiy
 */
@Component
public class InterviewerValidator implements ConstraintValidator<ExistedInterviewer, com.interview.model.Interviewer> {

    @Autowired
    InterviewerRepository repository;

    @Override
    public void initialize(ExistedInterviewer interviewer) {}

    @Override
    public boolean isValid(final com.interview.model.Interviewer interviewer, ConstraintValidatorContext constraintValidatorContext) {
        return interviewer.getId() != null && repository.findOne(interviewer.getId()) != null;
    }
}
